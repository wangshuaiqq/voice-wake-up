package com.realtime.voice.util;

import java.io.IOException;

import javax.websocket.Session;

import org.json.JSONException;
import org.json.JSONObject;

import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;

/**
 * 百度实时语音识别监听类
 * 
 * @author park_
 *
 */
@Slf4j
public class Wlistener extends WebSocketListener {

	private Session session;

	public Wlistener(Session session) {
		this.session = session;
	}

	@Override
	public void onOpen(WebSocket webSocket, Response response) {
		super.onOpen(webSocket, response);
		new Thread(() -> {
			try {
				sendStartFrame(webSocket);
			} catch (JSONException e) {
				log.info(e.getClass().getSimpleName(), e);
				throw new RuntimeException(e);
			}
		}).start();
	}

	@Override
	public void onMessage(WebSocket webSocket, String text) {
		super.onMessage(webSocket, text);
		if (text.contains("\"TYPE_HEARTBEAT\"")) {
            log.info("receive heartbeat: " + text.trim());
        } else {
            log.info("receive text: " + text.trim());
        }
		try {
        	cn.hutool.json.JSONObject json = JSONUtil.parseObj(text.trim());
        	System.err.println(json);
        	if (json.get("err_msg") != null && "OK".equals(json.getStr("err_msg"))) {
        			session.getBasicRemote().sendText(json.toJSONString(0));
        	}
        } catch (IOException e) {
        	e.printStackTrace();
        }
	}

	@Override
	public void onClosing(WebSocket webSocket, int code, String reason) {
		super.onClosing(webSocket, code, reason);
		log.info("websocket event closing :" + code + " | " + reason);
		// 客户端关闭
		webSocket.close(1000, "");
	}

	@Override
	public void onClosed(WebSocket webSocket, int code, String reason) {
		super.onClosed(webSocket, code, reason);
		log.info("websocket closed: " + code + " | " + reason);
	}

	@Override
	public void onFailure(WebSocket webSocket, Throwable t, Response response) {
		super.onFailure(webSocket, t, response);
		log.info("websocket failure", t);
	}

	/**
	 * 发送开始参数帧
	 * 
	 * @param webSocket
	 * @throws JSONException
	 */
	private void sendStartFrame(WebSocket webSocket) throws JSONException {

		JSONObject params = new JSONObject();

		params.put("appid", Const.APPID);
		params.put("appkey", Const.APPKEY);

		params.put("dev_pid", Const.DEV_PID);
		params.put("cuid", "self_defined_server_id_like_mac_address");

		params.put("format", "pcm");
		params.put("sample", 16000);

		JSONObject json = new JSONObject();
		json.put("type", "START");
		json.put("data", params);
		log.info("send start FRAME:" + json.toString());
		webSocket.send(json.toString());
	}

}
