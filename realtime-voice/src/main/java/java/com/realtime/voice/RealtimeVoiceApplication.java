package com.realtime.voice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RealtimeVoiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(RealtimeVoiceApplication.class, args);
	}

}
