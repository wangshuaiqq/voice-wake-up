var voiceSign = false;
var msgIndex;
var speechTimeOut;
$(function() {
	initRecorder();
});

function initRecorder() {
	Recorder.get({
		wsPath: WS_PATH,
		sampleBits: 16,
		sampleRate: 16000
	}, function(rec) {
		console.info(new Date().format("yyyy-MM-dd hh:mm:ss"), '语音唤醒功能已启动，唤醒词为：', REALTIME_VOICE.KEYWORDS);
		Recorder.refreshSocket(rec, 1000*60*58, WS_PATH, function() {
			console.info(new Date().format("yyyy-MM-dd hh:mm:ss"), '连接已重置');
		}, function(data) {
			discern(data);
		});
	}, function(data) {
		discern(data);
	});
}

/**
 * 识别语音
 * @param {Object} data
 */
function discern(data) {
	console.info(new Date().format("yyyy-MM-dd hh:mm:ss"), '接收消息：', data);
	if (data.type == 'FIN_TEXT') {
		mateRouse(data.result);
	} else if (data.type == 'MID_TEXT') {
		if (msgIndex) {
			$('.layui-layer-content').text(data.result);
		}
	}
}

function initMsg(msg) {
	msgIndex = layer.msg(msg, {
		shade: 0.6,
		time: 0,
		end: function() {
			msgIndex = null;
		}
	});
}

function removeSpecialCharts(str) {
	return str.replace(/[\ |\~|\`|\!|\@|\#|\$|\%|\^|\&|\*|\(|\)|\-|\_|\+|\=|\||\\|\[|\]|\{|\}|\;|\:|\"|\'|\,|\<|\.|\>|\/|\?|，|。|？|！]/g,"");
}

function mateRouse(str) {
	console.info(new Date().format("yyyy-MM-dd hh:mm:ss"), '识别文本：', str);
	str = removeSpecialCharts(str);
	var pinyin = str.hanzi2pinyin({hyphen:''});
	var keyWords = REALTIME_VOICE.KEYWORDS;
	var status = false;
	mate: for (var i = 0; i < keyWords.length; i++) {
		if (str.indexOf(keyWords[i]) != -1 || pinyin.indexOf(keyWords[i]) != -1) {
			status = true;
			break mate;
		}
	}
	if (status) {
		// 监听到唤醒词
		voiceSign = true;
		if (!msgIndex) {
			initMsg('我在，请说出语音指令');
		}
		console.info(new Date().format("yyyy-MM-dd hh:mm:ss"), '检测到语音唤醒词');
		if (speechTimeOut) {
			clearTimeout(speechTimeOut);
		}
		speechTimeOut = setTimeout(function() {
			voiceSign = false;
			if (msgIndex) {
				layer.close(msgIndex);
			}
		}, 20000);
	} else {
		// 普通
		if (voiceSign) {
			// 识别指令
			if (str.indexOf('打开百度') != -1 || pinyin.indexOf('dakaibaidu') != -1) {
				$('.layui-layer-content').text('指令识别成功，即将打开百度...');
				console.info(new Date().format("yyyy-MM-dd hh:mm:ss"), '指令识别成功');
				setTimeout(function() {
					window.open('https://www.baidu.com/', '_blank');
				}, 1000)
			} else {
				$('.layui-layer-content').text('指令识别失败');
				console.info(new Date().format("yyyy-MM-dd hh:mm:ss"), '指令识别失败');
			}
			voiceSign = false;
			setTimeout(function() {
				if (msgIndex) {
					layer.close(msgIndex);
				}
			}, 1500);
		}
	}
}