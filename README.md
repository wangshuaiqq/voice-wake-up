# 语音唤醒

#### 介绍
pc端网页语音唤醒、语音识别、语音助手解决方案，基于百度实时语音识别实现，采用websocket实时传输麦克风音频

#### 项目说明
修改java包com.realtime.voice.util下Const.java 
appid appkey
appid,appkey申请方法请前往百度开放平台自行查看
浏览器请使用chrome内核的浏览器，尽量使用新版本

#### 项目主要逻辑

1、初始化监听麦克风
![输入图片说明](image.png)
2、获取麦克风音频流，并通过websocket传输到后端
![输入图片说明](image2.png)
